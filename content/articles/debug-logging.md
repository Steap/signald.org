# Debug Logging

signald's debug logging can be enabled with a command line flag (`-v`) or environment variable (`SIGNALD_VERBOSE_LOGGING=true`).
The rest of this page explains how to do that for various installation methods

## Container (docker)
set environment variable to your `docker run` line with `-e SIGNALD_VERBOSE_LOGGING=true`

## systemd (apt installation)
put `SIGNALD_VERBOSE_LOGGING=true` in `/etc/default/signald`, creating the file if needed, then restart signald (`sudo systemctl restart signald`)
